angular.module('starter', ['ionic','ngCordova', 'starter.LoginCtrl', 'starter.HomePageController', 'starter.BuilderRegCtrl', 'starter.HomeloanController','starter.BuilderTeamCtrl','starter.BuilderProfileCtrl','starter.TeamRegCtrl','starter.QuickOverviewCtrl','starter.ProjectdetailsCtrl','starter.SalesTeamReportCtrl','starter.CashflowController','starter.SalesteamController','starter.SalesteamprofileController','starter.MyleadsController','starter.NavController','starter.PaymentdemandController','starter.BankcodeController','starter.AnnualcashflowController','starter.CustomersController','starter.ProjectCashFlowCtrl','starter.ProjectcontinuesalesCtrl','starter.ProjectcontinueprogressCtrl','starter.consumerSalesReportCtrl','starter.ProjectsliderController','starter.ReportprofileController','starter.IncentiveplanController','starter.CashflowcalenderController','starter.BankwisereportController','starter.BankwisedetailsController','starter.VaastuController','starter.MailboxController','starter.AdvertisementsController','starter.SndSearchUserCtrl','starter.SndEnquiryCtrl','starter.SndHomePageController','starter.SndProjectDetailsCtrl','starter.SndUnitPlanCtrl','starter.SndProjectConSalesCtrl','starter.SndProjectSalesExeCtrl','starter.SndSalesCampaignCtrl','starter.thrdSearchUserCtrl','starter.ExeProfileCtrl','starter.ExeProjectDetailsCtrl','starter.ExeProjectSalesReportCtrl','starter.thrdconsumerSalesReportCtrl','starter.ExeEnquiryCtrl','starter.ExeSalesCampaignCtrl','starter.FrthHomePageController','starter.FrthProjectDetailsCtrl','starter.FrthUnitPlanCtrl','starter.FrthProjectConSalesCtrl','starter.SndteamviewCtrl','starter.thrdteamviewCtrl','starter.frthteamviewCtrl','starter.fstEnquiryCtrl','starter.EnquiryCtrl','starter.SearchCustomerCtrl','starter.TrackbuilderuserCtrl','starter.frthconsumerSalesReportCtrl','starter.passwordCtrl','starter.RefprofileCtrl'])

	.factory('Preferredbank', function(){
		var pbanks = [];
		return pbanks;
	})
	.factory('Dsapatener', function(){
		var dpartners = [];
		return dpartners;
	})
	.factory('Dsabankcodes', function(){
		var dcodes = [];
		return dcodes;
	})
	.factory('ExistingCode', function(){
        var bankid = [];
        return bankid;
    })

	.run(function($ionicPlatform, $ionicPopup,  $ionicHistory,$state,$http) {
		
		/*$ionicPlatform.registerBackButtonAction(function(e) {
			alert("You can not close the app");
            e.preventDefault();
        }, 1000);
		
		$ionicPlatform.onHardwareBackButton(function() {
			alert("Hey No yar!!....I beg you!!");
			$state.go("^");
		});*/
		
		
		delete $http.defaults.headers.common['X-Requested-With'];
	    $ionicPlatform.registerBackButtonAction(function(event) {
            if ($state.current.name=="fst.builderprofile" || $state.current.name=="thrd.ExeProfile" || $state.current.name=="snd.home" || $state.current.name=='frth.home' ) { // your check here
                  ionic.Platform.exitApp();

            }else{
               if ($ionicHistory.backView()) {
                    $ionicHistory.goBack();
                    //$ionicHistory.clearHistory();
                }else{
                     if ($state.current.name== "home" ) {
                        ionic.Platform.exitApp();
                     }else{


                      }
                }
            }


          }, 100);
	  $ionicPlatform.ready(function() {
		if (window.cordova && window.cordova.plugins.Keyboard) {
		  cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		  cordova.plugins.Keyboard.disableScroll(true);

		}
		if (window.StatusBar) {
		  // org.apache.cordova.statusbar required
		  StatusBar.styleDefault();
		}

		if(window.Connection) {
			if(navigator.connection.type == Connection.NONE) {
				$ionicPopup.confirm({
					title: "WhatsLoan",
					content: "Please check internet connection."
				})
				.then(function(result) {
					if(!result) {
						ionic.Platform.exitApp();
					}
				});
			}
		}

	  })

	})

    .config(['$stateProvider', '$urlRouterProvider','$cordovaCameraProvider','$httpProvider','$compileProvider', function($stateProvider, $urlRouterProvider, $cordovaCameraProvider, $httpProvider, $compileProvider) {
		$httpProvider.defaults.useXDomain = true;
	    delete $httpProvider.defaults.headers.common['X-Requested-With'];
	    $httpProvider.defaults.headers.common["Accept"] = "application/json";
	    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
		
		$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|sms|skype|tel|whatsapp|chrome-extension):/);
		
        $stateProvider
            .state('root', {
                cache: false,
                url : '/root',
                templateUrl : 'templates/root.html',
                controller : 'LoginCtrl'
            })
			.state('home', {
                url : '/home',
                templateUrl : 'templates/home.html',
                controller : 'HomePageController'
            })
			.state('register', {
			    cache: false,
                url : '/register',
                templateUrl : 'templates/register.html',
                controller : 'BuilderRegCtrl'
            })
            .state('otp', {
			    cache: false,
                url : '/otp',
                templateUrl : 'otp.html',
                controller : 'LoginCtrl'
            })
			.state('forgotpassword', {
			    cache: false,
                url : '/forgotpassword',
                templateUrl : 'templates/forgotpassword.html',
				controller : 'passwordCtrl'
            })
			
			.state('fifth.profilemm', {
			    cache: false,
                url : '/profile',
                templateUrl : 'templates/fifth-profile.html'
            })
			
			 /*.state('fifth.profile', {
                cache: false,
                url: '/fifthprofile',
                views: {
                    'fifth': {
                        templateUrl: 'templates/fifth-profile.html'

                    }
                }
            })*/
			
/***************************************************************************************************/
            .state('fst', {
                cache: false,
                url : '/fst',
                templateUrl : 'templates/fst-abstract.html',
                abstract : true
            })
            .state('fst.builderprofile', {
                //cache: false,
                url: '/builderprofile',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-builderprofile.html',
                        controller : 'BuilderProfileCtrl'

                    }
                }
            })

            .state('fst.builderTeam', {
                cache: false,
                url: '/builderTeam',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-builderTeam.html'
                    }
                }
            })
            .state('fst.salesTeamDetails', {
                cache: false,
                url: '/salesTeamDetails',
                views: {
                    'fst': {
                        templateUrl: 'fst-salesTeamDetails.html'
                    }
                }
            })
            .state('fst.salesTeamLocation', {
                //cache: false,
                url: '/salesTeamLocation',
                views: {
                    'fst': {
                        templateUrl: 'fst-salesTeamLocation.html'
                    }
                }
            })
			
			.state('fst.pendingDetails', {
                url: '/pendingDetails',
                views: {
                    'fst': {
                        templateUrl: 'fst-pendingDetails.html'
                    }
                }
            })
			
			.state('snd.salesTeamLocation', {
                //cache: false,
                url: '/salesTeamLocation',
                views: {
                    'snd': {
                        templateUrl: 'snd-salesTeamLocation.html'
                    }
                }
            })

			.state('thrd.salesTeamLocation', {
                //cache: false,
                url: '/salesTeamLocation',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-salesTeamLocation.html'
                    }
                }
            })
			
			.state('thrd.salesteam', {
			     cache: false,
                url: '/salesteam',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-salesteam.html'
                    }
                }
            })
			
			.state('thrd.SiteVisit', {
                url: '/SiteVisit',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-SiteVisit.html'

                    }
                }
            })
			
			.state('thrd.homeloan', {
			     cache: false,
                url: '/homeloan',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-homeloan.html'
                        
                    }
                }
            })

			
			.state('frth.salesTeamLocation', {
                //cache: false,
                url: '/salesTeamLocation',
                views: {
                    'frth': {
                        templateUrl: 'frth-salesTeamLocation.html'
                    }
                }
            })


			.state('fst.month', {
                url: '/month',
                views: {
                    'fst': {
                        templateUrl: 'fst-month.html'
                    }
                }
            }).state('fst.quarter', {
                url: '/quarter',


                views: {
                    'fst': {
                        templateUrl: 'fst-quarter.html'
                    }
                }

            }).state('fst.annuual', {
                url: '/annuual',

                views: {
                    'fst': {
                        templateUrl: 'fst-annuual.html'

                    }
                }
            })



            .state('fst.selectbank', {
                url: '/selectbank',
                views: {
                    'fst': {
                        templateUrl: 'fst-selectbank.html'
                    }
                }
            })
            .state('fst.idproof', {
                 cache: false,
                url: '/idproof',
                views: {
                    'fst': {
                        templateUrl: 'fst-idopen.html'
                    }
                }
            })
	      .state('fst.approved', {
                url: '/approved',
                views: {
                    'fst': {
                        templateUrl: 'fst-approved.html'
                    }
                }
            })
            .state('fst.allbank', {
                url: '/allbank',
                views: {
                    'fst': {
                        templateUrl: 'fst-allbank.html'
                    }
                }
            })
             .state('fst.bank-code', {
                url: '/bank-code',
                views: {
                    'fst': {
                        templateUrl: 'fst-bank-code.html'
                    }
                }
            })

             .state('fst.exist-bank-code', {
                url: '/exist-bank-code',
                views: {
                    'fst': {
                        templateUrl: 'fst-exist-bank-code.html'
                    }
                }
            })
            .state('fst.yearselect', {
                url: '/yearselect',
                views: {
                    'fst': {
                        templateUrl: 'fst-yearselect.html'
                    }
                }
            })
            .state('fst.apartment', {
                url: '/apartment',
                views: {
                    'fst': {
                        templateUrl: 'fst-apartment.html'
                    }
                }
            })

            .state('fst.registerteam', {
                cache: false,
                url: '/registerteam',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-registerteam.html'
                    }
                }
            })

            .state('fst.quickoverview', {
                url: '/quickoverview',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-quickoverview.html'
                    }
                }
            })
             .state('fst.projectphoto', {
                 cache: false,
                url: '/projectphoto',
                views: {
                    'fst': {
                        templateUrl: 'fst-projectphoto.html'
                    }
                }
            })
			.state('fst.projectdetails', {
                url: '/projectdetails',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-projectdetails.html'
                    }
                }
            })
			.state('fst.usersearch', {
                url: '/usersearch',

                views: {
                    'fst': {
                        templateUrl: 'templates/fst-usersearch.html',
                        controller : 'SearchCustomerCtrl'



                    }
                }
            })
			.state('fst.report', {
			     cache: false,
                url: '/report',
                views: {
                    'fst': {
                        templateUrl: 'fst-report.html'
                    }
                }
            })
			.state('fst.cashflow', {
			     cache: false,
                url: '/cashflow',
                views: {
                    'fst': {
                        templateUrl: 'fst-cashflow.html'
                    }
                }
            })
			.state('fst.salesteam', {
			     cache: false,
                url: '/salesteam',
                views: {
                    'fst': {
                        templateUrl: 'fst-salesteam.html'
                    }
                }
            })
			.state('fst.salesteamprofile', {
			     cache: false,
                url: '/salesteamprofile',
                views: {
                    'fst': {
                        templateUrl: 'fst-salesteamprofile.html'
                    }
                }
            })
            .state('fst.bookinglist', {
                 cache: false,
                url: '/bookinglist',
                views: {
                    'fst': {
                        templateUrl: 'fst-bookinglist.html'
                    }
                }
            })
            .state('fst.demandnote', {
                 cache: false,
                url: '/demandnote',
                views: {
                    'fst': {
                        templateUrl: 'fst-demandnote.html'
                    }
                }
            })
            .state('fst.demandnoteimage', {
                 cache: false,
                url: '/demandnoteimage',
                views: {
                    'fst': {
                        templateUrl: 'fst-demandnoteimage.html'
                    }
                }
            })
			.state('fst.myleads', {
			     cache: false,
                url: '/myleads',
                views: {
                    'fst': {
                        templateUrl: 'fst-myleads.html'
                    }
                }
            })
			.state('fst.addlead', {
			     cache: false,
                url: '/fstenquiry',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-addlead.html'
                    }
                }
            })
			.state('fst.homeloan', {
			     cache: false,
                url: '/homeloan',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-homeloan.html',
                        controller : 'HomeloanController'
                    }
                }
            })
			
			
			.state('fst.paymentdemand', {
			    cache: false,
                url: '/homeloan',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-paymentdemand.html',
                        controller : 'PaymentdemandController'
                    }
                }
            })
			.state('fst.bankcode', {
                url: '/bankcode',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-bankcode.html',
                        controller : 'BankcodeController'
                    }
                }
            })
			.state('fst.annualcashflow', {
                url: '/annualcashflow',
                views: {
                    'fst': {
                        templateUrl: 'fst-annualcashflow.html',
                        controller : 'ProjectCashFlowCtrl'
                    }
                }
            })
			.state('fst.customers', {
			    cache: false,
                url: '/customers',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-customers.html',
                        controller : 'CustomersController'
                    }
                }
            })
			.state('fst.projectcontinuesales', {
			    cache: false,
                url: '/projectcontinuesales',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-projectcontinuesales.html',
                        controller : 'ProjectcontinuesalesCtrl'
                    }
                }
            })
			 .state('fst.projectcashflow', {
			    cache: false,
                url: '/projectcashflow',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-projectcashflow.html'
                    }
                }
            })

		.state('fst.projectcontinueprogress', {
                cache: false,
                url: '/projectcontinueprogress',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-projectcontinueprogress.html'
                    }
                }
            })
            .state('fst.salesTeamReport', {
                cache: false,
                url: '/salesTeamReport',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-salesTeamReport.html'
                    }
                }
            })
			.state('fst.projectslider', {
			    cache: false,
                url: '/projectslider',
                views: {
                    'fst': {
                        templateUrl: 'fst-projectslider.html'
                    }
                }
            })
            .state('fst.projectstatus', {
                cache: false,
                url: '/projectstatus',
                views: {
                    'fst': {
                        templateUrl: 'fst-projectstatus.html'
                    }
                }
            })
			.state('fst.reportprofile', {
			    cache: false,
                url: '/reportprofile',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-reportprofile.html',
                        controller : 'ReportprofileController'
                    }
                }
            })
            .state('fst.reportTeamList', {
                cache: false,
                url: '/reportTeamList',
                views: {
                    'fst': {
                        templateUrl: 'fst-reportTeamList.html'
                    }
                }
            })
            .state('fst.reportTeamProfile', {
                //cache: false,
                url: '/reportTeamProfile',
                views: {
                    'fst': {
                        templateUrl: 'fst-reportTeamProfile.html'
                    }
                }
            })
            .state('fst.incentivePlan', {
                cache: false,
                url: '/incentivePlan',
                views: {
                    'fst': {
                        templateUrl: 'fst-incentivePlan.html'
                    }
                }
            })
            .state('fst.earnPlan', {
                 cache: false,
                url: '/earnPlan',
                views: {
                    'fst': {
                        templateUrl: 'fst-earnPlan.html'
                    }
                }
            })
			.state('fst.incentiveplan', {
                url: '/incentiveplan',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-incentiveplan.html',
                        controller : 'IncentiveplanController'
                    }
                }
            })
			.state('fst.cashflowcalender', {
			    cache: false,
                url: '/cashflowcalender',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-cashflowcalender.html',
                        controller : 'CashflowcalenderController'
                    }
                }
            })
			
			.state('fst.priceWaiver', {
                cache: false,
                url: '/priceWaiver',
                views: {
                    'fst': {
                        templateUrl: 'fst-priceWaiver.html'
                    }
                }
            })
			.state('fst.bankwisereport', {
			    cache: false,
                url: '/bankwisereport',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-bankwisereport.html'
                    }
                }
            })
			.state('snd.bankwisereport', {
			    cache: false,
                url: '/bankwisereport',
                views: {
                    'snd': {
                        templateUrl: 'templates/snd-bankwisereport.html'
                    }
                }
            })
			
			
			.state('snd.homeloan', {
			     cache: false,
                url: '/homeloan',
                views: {
                    'snd': {
                        templateUrl: 'snd-homeloan.html'
                        
                    }
                }
            })
			.state('thrd.bankwisereport', {
			    cache: false,
                url: '/bankwisereport',
                views: {
                    'thrd': {
                        templateUrl: 'templates/thrd-bankwisereport.html'
                    }
                }
            })
			.state('frth.bankwisereport', {
			    cache: false,
                url: '/bankwisereport',
                views: {
                    'frth': {
                        templateUrl: 'templates/frth-bankwisereport.html'
                    }
                }
            })
			.state('fst.bankwisedetails', {
			    cache: false,
                url: '/bankwisedetails',
                views: {
                    'fst': {
                        templateUrl: 'fst-bankwisedetails.html'
                    }
                }
            })
			.state('snd.bankwisedetails', {
			    cache: false,
                url: '/bankwisedetails',
                views: {
                    'snd': {
                        templateUrl: 'snd-bankwisedetails.html'
                    }
                }
            })
			.state('thrd.bankwisedetails', {
			    cache: false,
                url: '/bankwisedetails',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-bankwisedetails.html'
                    }
                }
            })
			.state('frth.bankwisedetails', {
			    cache: false,
                url: '/bankwisedetails',
                views: {
                    'frth': {
                        templateUrl: 'frth-bankwisedetails.html'
                    }
                }
            })
			.state('fst.vaastu', {
                url: '/vaastu',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-vaastu.html',
                        controller : 'VaastuController'
                    }
                }
            })
			.state('fst.mailbox', {
                url: '/mailbox',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-mailbox.html',
                        controller : 'MailboxController'
                    }
                }
            })
			.state('fst.advertisements', {
                url: '/advertisements',
                views: {
                    'fst': {
                        templateUrl: 'templates/fst-advertisements.html',
                        controller : 'AdvertisementsController'
                    }
                }
            })
			
			.state('fst.masterplan', {
                url: '/masterplan',
                views: {
                    'fst': {
                        templateUrl: 'fst-masterplan.html'
                    
                    }
                }
            })
			
			.state('fst.unitfloorplan', {
                url: '/unitfloorplan',
                views: {
                    'fst': {
                        templateUrl: 'fst-unitfloorplan.html'

                    }
                }
            })
            .state('fst.unitplan', {
                url: '/unitplan',
                views: {
                    'fst': {
                        templateUrl: 'fst-unitplan.html'

                    }
                }
            })
			
			.state('fst.allbooking', {
                url: '/allbooking',
                views: {
                    'fst': {
                        templateUrl: 'fst-allbooking.html'
                    }
                }
            })
            
            .state('fst.SearchUnitAvailability', {
                url: '/SearchUnitAvailability',
                views: {
                    'fst': {
                        templateUrl: 'fst-SearchUnitAvailability.html'

                    }
                }
            })
			
			 .state('fst.customerBookingDetails', {
                url: '/customerBookingDetails',
                views: {
                    'fst': {
                        templateUrl: 'fst-customerBookingDetails.html'

                    }
                }
            })
			
			.state('fst.customerlist', {
                url: '/customerlist',
                views: {
                    'fst': {
                        templateUrl: 'fst-customerlist.html'

                    }
                }
            })
			
			.state('fst.SiteVisit', {
                url: '/SiteVisit',
                views: {
                    'fst': {
                        templateUrl: 'fst-SiteVisit.html'

                    }
                }
            })
			
			.state('fst.customerview', {
                cache: false,
                url: '/customerview',
                views: {
                    'fst': {
                        templateUrl: 'fst-customerview.html'

                    }
                }
            })
			
			.state('dsapartner', {
			  url: '/dsapartner',
			  templateUrl: 'templates/fst-dsapartner.html'
			})
			.state('preferredbank', {
			  url: '/preferredbank',
				  templateUrl: 'templates/fst-preferredbank.html'
			})
			.state('bankcode', {
			  url: '/bankcode',
			  templateUrl: 'templates/fst-bankcode.html'
			})

/***************************************************************************************************/
            .state('snd', {
                url : '/snd',
                templateUrl : 'templates/snd-abstract.html',
                abstract : true
            })
            .state('snd.home', {
                cache: false,
                url: '/home',
                views: {
                    'snd': {
                        templateUrl: 'templates/snd-home.html'
                    }
                }
            })
			.state('snd.psmteam', {
				cache: false,
                url: '/psmteam',
                views: {
                    'snd': {
                        templateUrl: 'templates/snd-psmteam.html'
						//controller : 'SndteamviewCtrl'
                    }
                }
            })
			
			.state('thrd.pscteam', {
				cache: false,
                url: '/pscteam',
                views: {
                    'thrd': {
                        templateUrl: 'templates/thrd-pscteam.html'
					
                    }
                }
            })
			
			.state('frth.pscteam', {
				cache: false,
                url: '/pscteam',
                views: {
                    'frth': {
                        templateUrl: 'templates/frth-pscteam.html'
					
                    }
                }
            })
			
             .state('snd.consumersfollowupdetails', {
                url: '/consumersfollowupdetails',
                views: {
                    'snd': {
                        templateUrl: 'snd-consumersfollowupdetails.html'
                    }
                }
            })
            .state('snd.idproof', {
                url: '/idproof',
                views: {
                    'snd': {
                        templateUrl: 'snd-idproof.html'
                    }
                }
            })
            .state('snd.bookingInterest', {
                url: '/bookingInterest',
                views: {
                    'snd': {
                        templateUrl: 'snd-bookingInterest.html'
                    }
                }
            })
            .state('snd.bookingInterestCustomer', {
                url: '/bookingInterestCustomer',
                views: {
                    'snd': {
                        templateUrl: 'snd-bookingInterestCustomer.html'
                    }
                }
            })
            .state('snd.consumersBookingInterestdetails', {
                url: '/consumersBookingInterestdetails',
                views: {
                    'snd': {
                        templateUrl: 'snd-consumersBookingInterestdetails.html'
                    }
                }
            })
            .state('snd.usersearch', {
                cache: false,
                url: '/usersearch',
                views: {
                    'snd': {
                        templateUrl: 'templates/snd-usersearch.html',
                        controller: 'SearchCustomerCtrl'

                    }
                }
            })
            .state('snd.searchCustomer', {
                url: '/searchCustomer',
                views: {
                    'snd': {
                        templateUrl: 'snd-searchCustomer.html'
                    }
                }
            })
             .state('snd.addLead', {
                  cache: false,
                 url: '/addLead',
                 views: {
                     'snd': {
                         templateUrl: 'templates/snd-addLead.html'
                     }
                 }
              })

            .state('snd.leadFollowUpProject', {
                url: '/leadFollowUpProject',
                views: {
                    'snd': {
                        templateUrl: 'snd-leadFollowUpProject.html'

                    }
                }
            })
            .state('snd.EnquiryCustomer', {
                url: '/EnquiryCustomer',
                views: {
                    'snd': {
                        templateUrl: 'snd-EnquiryCustomer.html'

                    }
                }
            })
            .state('snd.FollowUpProject', {
                url: '/FollowUpProject',
                views: {
                    'snd': {
                        templateUrl: 'snd-FollowUpProject.html'

                    }
                }
            })
            .state('snd.visitCustomer', {
                url: '/visitCustomer',
                views: {
                    'snd': {
                        templateUrl: 'snd-visitCustomer.html'

                    }
                }
            })

            .state('snd.projectdetails', {
                url: '/projectdetails',
                views: {
                    'snd': {
                        templateUrl: 'templates/snd-projectdetails.html',
                        controller : 'SndProjectDetailsCtrl'
                    }
                }
            })
            .state('snd.unitplan', {
                url: '/unitplan',
                views: {
                    'snd': {
                        templateUrl: 'templates/snd-unitplan.html',
                        controller : 'SndUnitPlanCtrl'
                    }
                }
            })
			
			.state('snd.allbooking', {
                url: '/allbooking',
                views: {
                    'snd': {
                        templateUrl: 'snd-allbooking.html'

                    }
                }
            })
			
			.state('snd.salesteam', {
			     cache: false,
                url: '/salesteam',
                views: {
                    'snd': {
                        templateUrl: 'snd-salesteam.html'
                    }
                }
            })
			
			.state('snd.customerBookingDetails', {
                url: '/customerBookingDetails',
                views: {
                    'snd': {
                        templateUrl: 'snd-customerBookingDetails.html'

                    }
                }
            })
			
			
            .state('snd.projectcontinuesales', {
                url: '/projectcontinuesales',
                views: {
                    'snd': {
                        templateUrl: 'templates/snd-projectcontinuesales.html',
                        controller : 'SndProjectConSalesCtrl'
                    }
                }
            })
            .state('snd.projectcontinuesalesexe', {
                url: '/projectcontinuesalesexe',
                views: {
                    'snd': {
                        templateUrl: 'templates/snd-projectcontinuesalesexe.html',
                        controller : 'SndProjectSalesExeCtrl'
                    }
                }
            })
            .state('snd.salesExeLocation', {
                url: '/salesExeLocation',
                views: {
                    'snd': {
                        templateUrl: 'snd-salesExeLocation.html'
                    }
                }
            })
            .state('snd.consumersalesrequest', {
                cache: false,
                url: '/consumersalesrequest',
                views: {
                    'snd': {
                        templateUrl: 'templates/snd-consumersalesrequest.html'
                    }
                }
            })
            .state('snd.pendingDetails', {
                url: '/pendingDetails',
                views: {
                    'snd': {
                        templateUrl: 'snd-pendingDetails.html'
                    }
                }
            })
            .state('snd.salescampaign', {
                url: '/salescampaign',
                views: {
                    'snd': {
                        templateUrl: 'templates/snd-salescampaign.html',
                        controller : 'SndSalesCampaignCtrl'
                    }
                }
            })
            .state('snd.incentive', {
                url: '/incentive',
                views: {
                    'snd': {
                        templateUrl: 'snd-incentive.html'
                    }
                }
            })
            .state('snd.ex-incentive', {
                url: '/ex-incentive',
                views: {
                    'snd': {
                        templateUrl: 'snd-ex-incentive.html'
                    }
                }
            })
            .state('snd.salesExecutive', {
                url: '/salesExecutive',
                views: {
                    'snd': {
                        templateUrl: 'snd-salesExecutive.html'
                    }
                }
            })
            .state('snd.salesProjectExecutive', {
                url: '/salesProjectExecutive',
                views: {
                    'snd': {
                        templateUrl: 'snd-salesProjectExecutive.html'
                    }
                }
            })
            .state('snd.paymentdemand', {
                url: '/paymentdemand',
                views: {
                    'snd': {
                        templateUrl: 'snd-paymentdemand.html'
                    }
                }
            })
            .state('snd.denoteimage', {
                url: '/denoteimage',


                views: {
                    'snd': {
                        templateUrl: 'snd-denoteimage.html'

                    }
                }
            })
            .state('snd.agreeimage', {
                cache: false,
                url: '/agreeimage',
                views: {
                    'snd': {



                        templateUrl: 'snd-agreeimage.html'
                    }

                }

            })
            .state('snd.salesCustomers', {
                cache: false,
                url: '/salesCustomers',

                views: {
                    'snd': {
                        templateUrl: 'snd-salesCustomers.html'

                    }
                }
            })
			
			.state('snd.unitfloorplan', {
                url: '/unitfloorplan',
                views: {
                    'snd': {
                        templateUrl: 'snd-unitfloorplan.html'

                    }
                }
            })
			
			.state('snd.unitplansnd', {
                url: '/unitplansnd',
                views: {
                    'snd': {
                        templateUrl: 'snd-unitplansnd.html'

                    }
                }
            })
			
			.state('snd.SearchUnitAvailability', {
                url: '/SearchUnitAvailability',
                views: {
                    'snd': {
                        templateUrl: 'snd-SearchUnitAvailability.html'

                    }
                }
            })
			
            .state('snd.salesBookoings', {
                url: '/salesBookoings',

                views: {
                    'snd': {
                        templateUrl: 'snd-salesBookoings.html'

                    }
                }
            })
            .state('snd.salesConsumers', {
                url: '/salesConsumers',
                views: {
                    'snd': {
                        templateUrl: 'snd-salesConsumers.html'
                    }
                }





            })
            .state('snd.customerdetails', {
                url: '/customerdetails',
                views: {
                    'snd': {
                        templateUrl: 'snd-customerdetails.html'

                    }
                }
            })
            .state('snd.report', {
                cache: false,
                url: '/report',

                views: {
                    'snd': {

                        templateUrl: 'snd-report.html'

                    }
                }
            })
            .state('snd.ex-report', {
                url: '/ex-report',
                views: {
                    'snd': {

                        templateUrl: 'snd-ex-report.html'

                    }
                }
            })
            .state('snd.projectbank', {
                url: '/projectbank',


                views: {
                    'snd': {

                        templateUrl: 'snd-projectbank.html'

                    }
                }
            })
			
			.state('snd.masterplan', {
                url: '/masterplan',
                views: {
                    'snd': {
                        templateUrl: 'snd-masterplan.html'

                    }
                }
            })
            .state('snd.demandnoteimg', {
                cache: false,
                url: '/demandnoteimg',


                views: {
                    'snd': {
                        templateUrl: 'snd-demandnoteimg.html'



                    }
                }
            })
            .state('snd.allfloorplan', {
                url: '/allfloorplan',
                views: {
                    'snd': {
                        templateUrl: 'snd-allfloorplan.html'



                    }
                }
            })
            .state('snd.campaignlist', {
                cache: false,
                url: '/campaignlist',
                views: {
                    'snd': {
                        templateUrl: 'snd-campaignlist.html'

                    }
                }
            })
			
			.state('snd.SiteVisit', {
                url: '/SiteVisit',
                views: {
                    'snd': {
                        templateUrl: 'snd-SiteVisit.html'

                    }
                }
            })
			
			
            .state('snd.campaigndetails', {

                url: '/campaigndetails',


                views: {
                    'snd': {
                        templateUrl: 'snd-campaigndetails.html'



                    }
                }
            })
            .state('snd.attendance', {
                cache: false,
                url: '/attendance',


                views: {
                    'snd': {

                        templateUrl: 'snd-attendance.html'

                    }
                }
            })
            .state('snd.campaignapproval', {
                cache: false,
                url: '/campaignapproval',


                views: {
                    'snd': {
                        templateUrl: 'snd-campaignapproval.html'



                    }
                }
            })
            .state('snd.campaignApprdetails', {
                cache: false,
                url: '/campaignApprdetails',
                views: {
                    'snd': {
                        templateUrl: 'snd-campaignApprdetails.html'



                    }
                }
            })
            .state('snd.tobeattendance', {
                cache: false,
                url: '/tobeattendance',


                views: {
                    'snd': {
                        templateUrl: 'snd-tobeattendance.html'



                    }
                }
            })
            .state('snd.AddCampaign', {
                cache: false,
                url: '/AddCampaign',
                views: {
                    'snd': {
                        templateUrl: 'snd-AddCampaign.html'



                    }
                }
            })
            .state('snd.priceWaiver', {
                cache: false,
                url: '/priceWaiver',
                views: {
                    'snd': {
                        templateUrl: 'snd-priceWaiver.html'
                    }
                }
            })
/***************************************************************************************************/
			.state('thrd', {
			    cache: false,
                url : '/thrd',
                templateUrl : 'templates/thrd-abstract.html',
                abstract : true
            })
            .state('thrd.ExeProfile', {
                cache: false,
                url: '/ExeProfile',
                views: {
                    'thrd': {
                        templateUrl: 'templates/thrd-ExeProfile.html'

                    }
                }
            })
            .state('thrd.idproof', {
                cache: false,
                url: '/idproof',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-idproof.html'

                    }
                }
            })
			.state('thrd.customerview', {
                cache: false,
                url: '/customerview',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-customerview.html'

                    }
                }
            })
			
            .state('thrd.BookingInterest', {
                url: '/BookingInterest',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-BookingInterest.html'
                    }
                }
            })
            .state('thrd.bookingInterestCustomer', {
                url: '/bookingInterestCustomer',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-bookingInterestCustomer.html'
                    }
                }
            })
            .state('thrd.consumersBookingInterestdetails', {
                url: '/consumersBookingInterestdetails',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-consumersBookingInterestdetails.html'
                    }
                }
            })
             .state('thrd.usersearch', {
                 cache: false,
                url: '/usersearch',
                views: {
                    'thrd': {
                        templateUrl: 'templates/thrd-usersearch.html',
                        controller: 'SearchCustomerCtrl'
                    }
                }
             })
             .state('thrd.searchCustomer', {
                url: '/searchCustomer',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-searchCustomer.html'
                    }
                }
             })
              .state('thrd.addLead', {
                  cache: false,
                 url: '/addLead',
                 views: {
                     'thrd': {
                         templateUrl: 'templates/thrd-addLead.html'
                     }
                 }
              })
            .state('thrd.FollowUpProject', {
                url: '/FollowUpProject',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-FollowUpProject.html'

                    }
                }
            })
			
            .state('thrd.FollowUpCustomer', {
                url: '/FollowUpCustomer',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-FollowUpCustomer.html'

                    }
                }
            })
            .state('thrd.consumersfollowupdetails', {
                url: '/consumersfollowupdetails',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-consumersfollowupdetails.html'
                    }
                }
            })
            // project details
           .state('thrd.ExeProjectDetails', {
               url: '/ExeProjectDetails',
               views: {
                   'thrd': {
                       templateUrl: 'templates/thrd-ExeProjectDetails.html'

                   }
               }
           })
           .state('thrd.approved', {
              url: '/approved',
              views: {
                  'thrd': {
                      templateUrl: 'thrd-approved.html'

                  }
              }
          })
          .state('thrd.customerlist', {
                url: '/customerlist',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-customerlist.html'

                    }
                }
            })
           .state('thrd.customerbydemandnote', {
                url: '/customerbydemandnote',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-customerbydemandnote.html'

                    }
                }
            })
            .state('thrd.demandnotes', {
                url: '/demandnotes',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-demandnotes.html'

                    }
                }
            })
             .state('thrd.allbooking', {
                url: '/allbooking',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-allbooking.html'

                    }
                }
            })
            .state('thrd.allbookingforagreements', {
                url: '/allbookingforagreements',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-allbookingforagreements.html'

                    }
                }
            })
            .state('thrd.agreements', {
                url: '/agreements',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-agreements.html'

                    }
                }
            })
            .state('thrd.unitfloorplan', {
                url: '/unitfloorplan',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-unitfloorplan.html'

                    }
                }
            })
            .state('thrd.unitplan', {
                url: '/unitplan',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-unitplan.html'

                    }
                }
            })
            .state('thrd.masterplan', {
                url: '/masterplan',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-masterplan.html'

                    }
                }
            })
            .state('thrd.SearchUnitAvailability', {
                url: '/SearchUnitAvailability',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-SearchUnitAvailability.html'

                    }
                }
            })
            .state('thrd.customerBookingDetails', {
                url: '/customerBookingDetails',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-customerBookingDetails.html'

                    }
                }
            })

          .state('thrd.incentivePlan', {
                url: '/incentivePlan',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-incentivePlan.html'

                    }
                }
            })
            .state('thrd.incentiveEarn', {
                url: '/incentiveEarn',
                views: {
                    'thrd': {
                        templateUrl: 'thrd-incentiveEarn.html'

                    }
                }
            })
          //sales reports
           .state('thrd.ExeProjectSalesReport', {
              url: '/ExeProjectSalesReport',
              views: {
                  'thrd': {
                      templateUrl: 'templates/thrd-ExeProjectSalesReport.html'

                  }
              }
          })
          .state('thrd.report', {
               url: '/report',
               views: {
                   'thrd': {
                       templateUrl: 'thrd-report.html'

                   }
               }
           })
           .state('thrd.cdemandnote', {
               url: '/cdemandnote',
               views: {
                   'thrd': {
                       templateUrl: 'thrd-cdemandnote.html'

                   }
               }
           })
          //Sales Campaign
          .state('thrd.ExeSalesCampaign', {
             url: '/ExeSalesCampaign',
             views: {
                 'thrd': {
                     templateUrl: 'templates/thrd-ExeSalesCampaign.html'

                 }
             }
         })
         .state('thrd.AddCampaign', {
             cache: false,
             url: '/AddCampaign',
             views: {
                 'thrd': {
                     templateUrl: 'thrd-AddCampaign.html'

                 }
             }
         })
         .state('thrd.ViewAllCampaign', {
             url: '/ViewAllCampaign',
             views: {
                 'thrd': {
                     templateUrl: 'thrd-ViewAllCampaign.html'

                 }
             }
         })

         .state('thrd.ViewSelectedCampaign', {
                     url: '/ViewSelectedCampaign',
                     views: {
                         'thrd': {
                             templateUrl: 'thrd-ViewSelectedCampaign.html'

                         }
                     }
                 })

         .state('thrd.attentedCamp', {
                     url: '/attentedCamp',
                     views: {
                         'thrd': {
                             templateUrl: 'thrd-attentedCamp.html'

                         }
                     }
          })


           //enquiry
          .state('thrd.Enquiry', {
              url: '/Enquiry',
              views: {
                  'thrd': {
                      templateUrl: 'templates/thrd-Enquiry.html'

                  }
              }
          })

        .state('thrd.consumersalesrequest', {
              cache: false,
              url: '/consumersalesrequest',
              views: {
                  'thrd': {
                      templateUrl: 'templates/thrd-consumersalesrequest.html'
                  }
              }
          })
          .state('thrd.pendingDetails', {
              cache: false,
              url: '/pendingDetails',
              views: {
                  'thrd': {
                      templateUrl: 'thrd-pendingDetails.html'
                  }
              }
          })
           .state('thrd.priceWaiver', {
              cache: false,
              url: '/priceWaiver',
              views: {
                  'thrd': {
                      templateUrl: 'thrd-priceWaiver.html'
                  }
              }
          })
/***************************************************************************************************/
			.state('frth', {
			    cache: false,
                url : '/frth',
                templateUrl : 'templates/frth-abstract.html',
                abstract : true
            })
            .state('frth.home', {
                cache: false,
                url: '/home',
                views: {
                    'frth': {
                        templateUrl: 'templates/frth-home.html'


                    }
                }
            })
			//Sales Campaign
          .state('frth.ExeSalesCampaign', {
             url: '/ExeSalesCampaign',
             views: {
                 'frth': {
                     templateUrl: 'templates/frth-ExeSalesCampaign.html'

                 }
             }
         })
         .state('frth.AddCampaign', {
             cache: false,
             url: '/AddCampaign',
             views: {
                 'frth': {
                     templateUrl: 'frth-AddCampaign.html'

                 }
             }
         })
         .state('frth.ViewAllCampaign', {
             url: '/ViewAllCampaign',
             views: {
                 'frth': {
                     templateUrl: 'frth-ViewAllCampaign.html'

                 }
             }
         })
		 
		 .state('frth.pendingDetails', {
              cache: false,
              url: '/pendingDetails',
              views: {
                  'frth': {
                      templateUrl: 'frth-pendingDetails.html'
                  }
              }
          })

         .state('frth.ViewSelectedCampaign', {
                     url: '/ViewSelectedCampaign',
                     views: {
                         'frth': {
                             templateUrl: 'frth-ViewSelectedCampaign.html'

                         }
                     }
                 })

         .state('frth.attentedCamp', {
                     url: '/attentedCamp',
                     views: {
                         'frth': {
                             templateUrl: 'frth-attentedCamp.html'

                         }
                     }
          })

		  
		  
		  
		  
		  
		  
            .state('frth.projectdetails', {
                url: '/projectdetails',


                views: {
                    'frth': {
                        templateUrl: 'templates/frth-projectdetails.html',
                        controller : 'FrthProjectDetailsCtrl'



                    }
                }
            })
			.state('frth.addLead', {
                url: '/addLead',


                views: {
                    'frth': {
                        templateUrl: 'templates/frth-addLead.html',
                        controller : 'FrthProjectDetailsCtrl'



                    }
                }
            })
			.state('frth.consumersalesrequest', {
                url: '/consumersalesrequest',


                views: {
                    'frth': {
                        templateUrl: 'templates/frth-consumersalesrequest.html',
                        controller : 'FrthProjectDetailsCtrl'



                    }
                }
            })
			.state('frth.usersearch', {
                url: '/usersearch',


                views: {
                    'frth': {
                        templateUrl: 'templates/frth-usersearch.html',
                        controller : 'SearchCustomerCtrl'



                    }
                }
            })
            .state('frth.unitplan', {
                url: '/unitplan',


                views: {
                    'frth': {
                        templateUrl: 'templates/frth-unitplan.html',
                        controller : 'FrthUnitPlanCtrl'



                    }
                }
            })
            .state('frth.projectcontinuesales', {
                url: '/projectcontinuesales',


                views: {
                    'frth': {
                        templateUrl: 'templates/frth-projectcontinuesales.html',
                        controller : 'FrthProjectConSalesCtrl'
                    }
                }
            })
			
			.state('frth.unitfloorplan', {
                url: '/unitfloorplan',
                views: {
                    'frth': {
                        templateUrl: 'frth-unitfloorplan.html'

                    }
                }
            })
			
			.state('frth.SiteVisit', {
                url: '/SiteVisit',
                views: {
                    'frth': {
                        templateUrl: 'frth-SiteVisit.html'

                    }
                }
            })
			
			.state('frth.unitplanshow', {
                url: '/unitplanshow',
                views: {
                    'frth': {
                        templateUrl: 'frth-unitplanshow.html'

                    }
                }
            })
			
			.state('frth.SearchUnitAvailability', {
                url: '/SearchUnitAvailability',
                views: {
                    'frth': {
                        templateUrl: 'frth-SearchUnitAvailability.html'

                    }
                }
            })



            .state('frth.projectbank', {
                url: '/projectbank',
                views: {
                    'frth': {
                        templateUrl: 'frth-projectbank.html'
                    }
                }
            })
            .state('frth.paymentdemand', {
                url: '/paymentdemand',


                views: {
                    'frth': {
                        templateUrl: 'frth-paymentdemand.html'



                    }
                }
            })
            .state('frth.denoteimage', {
                url: '/denoteimage',


                views: {
                    'frth': {
                        templateUrl: 'frth-denoteimage.html'



                    }
                }
            })
            .state('frth.agreeimage', {
                url: '/agreeimage',


                views: {
                    'frth': {

                        templateUrl: 'frth-agreeimage.html'

                    }
                }
            })
			
			
			.state('frth.homeloan', {
			     cache: false,
                url: '/homeloan',
                views: {
                    'frth': {
                        templateUrl: 'frth-homeloan.html'
                        
                    }
                }
            })
			
			.state('frth.salesteam', {
			     cache: false,
                url: '/salesteam',
                views: {
                    'frth': {
                        templateUrl: 'frth-salesteam.html'
                    }
                }
            })
            .state('frth.salesCustomers', {
                url: '/salesCustomers',


                views: {
                    'frth': {
                        templateUrl: 'frth-salesCustomers.html'

                    }
                }
            })
			
			.state('frth.projectleads', {
                url: '/projectleads',


                views: {
                    'frth': {
                        templateUrl: 'frth-projectleads.html'

                    }
                }
            })
			
			.state('snd.projectleads', {
                url: '/projectleads',


                views: {
                    'snd': {
                        templateUrl: 'snd-projectleads.html'

                    }
                }
            })
			.state('thrd.projectleads', {
                url: '/projectleads',


                views: {
                    'thrd': {
                        templateUrl: 'thrd-projectleads.html'

                    }
                }
            })
            .state('frth.salesBookoings', {
                url: '/salesBookoings',


                views: {
                    'frth': {
                        templateUrl: 'frth-salesBookoings.html'



                    }
                }
            })
			
			.state('frth.allbooking', {
                url: '/allbooking',
                views: {
                    'frth': {
                        templateUrl: 'frth-allbooking.html'

                    }
                }
            })
            .state('frth.salesConsumers', {
                url: '/salesConsumers',


                views: {
                    'frth': {
                        templateUrl: 'frth-salesConsumers.html'



                    }
                }
            })
            .state('frth.customerdetails', {
                url: '/customerdetails',


                views: {
                    'frth': {
                        templateUrl: 'frth-customerdetails.html'



                    }
                }
            })
            .state('frth.allfloorplan', {
                url: '/allfloorplan',



                views: {
                    'frth': {
                        templateUrl: 'frth-allfloorplan.html'



                    }
                }
            })
			
			
			.state('frth.masterplan', {
                url: '/masterplan',
                views: {
                    'frth': {
                        templateUrl: 'frth-masterplan.html'
                    
                    }
                }
            })
			
			
             .state('frth.incentive', {
                url: '/incentive',


                views: {
                    'frth': {

                        templateUrl: 'frth-incentive.html'

                    }
                }
            })
			
			.state('frth.customerBookingDetails', {
                url: '/customerBookingDetails',
                views: {
                    'frth': {
                        templateUrl: 'frth-customerBookingDetails.html'

                    }
                }
            })
			
			
            .state('frth.ex-incentive', {
                url: '/ex-incentive',
                views: {
                    'frth': {
                        templateUrl: 'frth-ex-incentive.html'
                    }
                }
            })
            .state('frth.salesExecutive', {
                url: '/salesExecutive',

                views: {
                    'frth': {
                        templateUrl: 'frth-salesExecutive.html'
                    }
                }
            })
             .state('frth.demandnoteimg', {
                url: '/demandnoteimg',

                views: {
                    'frth': {
                        templateUrl: 'frth-demandnoteimg.html'

                    }
                }
            })
             .state('frth.report', {
                url: '/report',

                views: {
                    'frth': {
                        templateUrl: 'frth-report.html'

                    }
                }
            })

         $urlRouterProvider.otherwise('/home');
    }])

.directive('mdToggle', function($ionicGesture, $timeout) {
	return {
		restrict: 'E',
    replace: 'true',
    require: '?ngModel',
    transclude: true,
		template:
    '<div class="flip-toggle">' +
    '<div ng-transclude></div>' +
    '<label class="toggle">' +
    '<input type="checkbox">' +
    '<div class="track">' +
    '<div class="handle"><span class="handle-label handle-label-a">YES</span><span class="handle-label handle-label-b">NO</span></div>' +
    '</div>' +
    '</label>' +
    '</div>',
		compile: function(element, attr) {
      var input = element.find('input');
      angular.forEach({
        'name': attr.name,
        'ng-value': attr.ngValue,
        'ng-model': attr.ngModel,
        'ng-checked': attr.ngChecked,
        'ng-disabled': attr.ngDisabled,
        'ng-true-value': attr.ngTrueValue,
        'ng-false-value': attr.ngFalseValue,
        'ng-change': attr.ngChange
      }, function(value, name) {
        if (angular.isDefined(value)) {
          input.attr(name, value);
        }
      });

      
      if(attr.toggleClass) {
        element[0].getElementsByTagName('label')[0].classList.add(attr.toggleClass);
      }

      return function($scope, $element, $attr) {
        var el, checkbox, track, handle;

        el = $element[0].getElementsByTagName('label')[0];
        checkbox = el.children[0];
        track = el.children[1];
        handle = track.children[0];

        var ngModelController = angular.element(checkbox).controller('ngModel');

        $scope.toggle = new ionic.views.Toggle({
          el: el,
          track: track,
          checkbox: checkbox,
          handle: handle,
          onChange: function() {
            if(checkbox.checked) {
              ngModelController.$setViewValue(true);
            } else {
              ngModelController.$setViewValue(false);
            }
            $scope.$apply();
          }
        });

        $scope.$on('$destroy', function() {
          $scope.toggle.destroy();
        });
      };
    }
	}
});


